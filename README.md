# Learn Elixir

To install it on Arch is quite simple

```bash
$ yay -S elixir
```

## Creating a project

Elixr comes with `mix` a tool designed to bootstrap Elixir projects

```
$ mix new cards
```

This creates a project called cards.

There's also an interactive shell called `iex`

```
iex -S mix
```
